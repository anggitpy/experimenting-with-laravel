<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hero extends Model
{
    protected $fillable = ['heroes_name', 'origin', 'alias', 'power_level', 'signature_move'];
}
