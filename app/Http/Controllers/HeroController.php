<?php

namespace App\Http\Controllers;

use App\Hero;
use Illuminate\Http\Request;

class HeroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title'] = 'Heroes Creation';
        $data['heroes'] = Hero::all();
        return view('create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'heroes_name' => 'required',
            'origin' => 'required',
            'alias' => 'nullable|string',
            'power_level' => 'required|numeric',
            'signature_move' => 'required'
        ]);
        $hero = Hero::create($validated);        
        $hero->save();
        return redirect('/heroes/create')->with('success', 'Heroes successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Hero  $hero
     * @return \Illuminate\Http\Response
     */
    public function show(Hero $hero)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Hero  $hero
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title'] = 'Edit Heroes';
        $data['hero'] = Hero::find($id);
        $data['heroes'] = Hero::all();
        return view('edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Hero  $hero
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {        
        $validated = $request->validate([
            'heroes_name' => 'required',
            'origin' => 'required',
            'alias' => 'nullable|string',
            'power_level' => 'required|numeric',
            'signature_move' => 'required'
        ]);
        Hero::where('id', $id)->update($validated);        
        return redirect('/heroes/create')->with('success', 'Heroes successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Hero  $hero
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hero = Hero::findOrFail($id);
        $hero->delete();

        return redirect('/heroes/create')->with('success', 'Heroes successfully deleted');
    }
}
