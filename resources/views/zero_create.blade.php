@extends('layouts.master')

@section('meta_csrf')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-3">

            @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
            @endif

            <div class="card">
                <div class="card-header">
                    Heroes
                </div>
                <div class="card-body">
                    <form action="{{ route('zeroes.store') }}" method="POST">

                        <div class="form-group mb-2">
                            <label>Name</label>
                            <input type="text" name="heroes_name" class="form-control @error('heroes_name') is-invalid @enderror" value="{{ old('heroes_name') }}" placeholder="Heroes Name" v-model="heroesName">
                            @error('heroes_name') <div class="text-danger mb-2"><small>{{ $message }}</small></div> @enderror
                        </div>
                        <div class="form-group mb-2">
                            <input type="text" name="origin" class="form-control @error('origin') is-invalid @enderror" value="{{ old('origin') }}" placeholder="Heroes Origin" v-model="origin">
                            @error('origin') <div class="text-danger mb-2"><small>{{ $message }}</small></div> @enderror
                        </div>
                        <div class="form-group mb-2">
                            <input type="text" name="alias" class="form-control @error('alias') is-invalid @enderror" value="{{ old('alias') }}" placeholder="Alias" v-model="alias">
                            @error('alias') <div class="text-danger mb-2"><small>{{ $message }}</small></div> @enderror
                        </div>
                        <div class="form-group">
                            <input type="number" name="power_level" class="form-control @error('power_level') is-invalid @enderror" value="{{ old('power_level') }}" placeholder="Power Level" v-model="powerLevel">
                            @error('power_level') <div class="text-danger mb-2"><small>{{ $message }}</small></div> @enderror
                        </div>
                        <div class="form-group">
                            <label>Signature</label>
                            <textarea name="signature_move" rows="3" class="form-control @error('signature_move') is-invalid @enderror" value="{{ old('signature_move') }}" placeholder="Signature Move" v-model="signatureMove"></textarea>
                            @error('signature_move') <div class="text-danger mb-2"><small>{{ $message }}</small></div> @enderror
                        </div>

                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </div>

        <!--
            This will be vuetable
        -->

    </div>
</div>
@endsection

@section('js')
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

<script>
    var vm = new Vue({
        el: '#app',
        data: {
            heroesName: '',
            origin: '',
            alias: '',
            powerLevel: '',
            signatureMove: ''
        },
        methods: {
            processForm() {

                var form = $('#form')
                var formData = new FormData(form[0])
                axios({
                        method: 'post',
                        url: "{{ site_url('leads/save') }}",
                        data: formData
                    })
                    .then(function(resp) {
                        console.log(resp)                        
                        vm.clearForm()
                    })
                    .catch(function(resp) {
                        console.log(resp)
                    })

            },
        }
    })
</script>

@endsection