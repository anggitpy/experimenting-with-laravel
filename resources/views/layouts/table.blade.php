<div class="col-lg-9">
    <div class="card">
        <div class="card-header">
            Heroes List    
        </div>
        <div class="card-body">
            <div class="table">
                <table class="table table-sm">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Origin</th>
                            <th>Alias</th>
                            <th>Level</th>
                            <th>Signature Move</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($heroes as $hero)
                        <tr>
                            <td>{{ $hero->id }}</td>
                            <td>{{ $hero->heroes_name }}</td>
                            <td>{{ $hero->origin }}</td>
                            <td>{{ $hero->alias }}</td>
                            <td>{{ $hero->power_level }}</td>
                            <td>{{ $hero->signature_move }}</td>
                            <td>
                                <a href="{{ route('heroes.edit', $hero->id) }}" class="btn btn-primary">Edit</a>
                                <form action="{{ route('heroes.destroy', $hero->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>